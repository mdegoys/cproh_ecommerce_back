var mongoose = require('mongoose');
var Schema = mongoose.Schema;

const cartSchema = new Schema({
  cart: Array,
  user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
});

const Cart = mongoose.model('Cart', cartSchema);

module.exports = Cart;
