# Description

  This repository is part of a school projet to create a mini e-commerce app. This is the back-end (API) part.

# Getting Started

  To use it, clone/fork and install the dependencies :

  `npm install`

  Then to start it, use the following command :

  `npm start`

# API description

  The API is three parts: products, users and carts.

  ## Products, on route /products

  *GET - /* 
  return a array containing the products

  ## Users, on route /users

  *POST - /login* - with username and password fields in the body\
  return a json as an answer : success or failure\
  in case of success also returns a token

  *POST - /register* - with username and password fields in the body\
  return a json as an answer : success or failure


  ## Carts, on route /carts, both asking for a valid token

  *GET - /:username*\ 
  return a json as an answer : success or failure\
  in case of success also returns a cart

  *POST - /:username*\
  return a json as an answer : success or failure

# Data Structure

  All files are in the root folder, with :
  - one model and one router file for the users
  - one model and one router file for the carts
  - everything else is in index.js
