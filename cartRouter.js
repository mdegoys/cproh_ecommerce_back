var express = require('express');
var jwt = require('jsonwebtoken');
var cartRouter = express.Router();
var Cart = require('./cartModel.js');
var User = require('./userModel');


function checkAuth(req, res, next) {
	try {
		const decoded = jwt.verify(req.headers.token, 'shhhhh')
		if (decoded.username != req.params.username) {
			throw new Error('You don\'t have the right to access to this cart data.')
		}
		next();
	} catch(error) {
		return res.status(401).json({
			'resultType': 'failure', 'resultMessage': error.message
		});
	}
}


cartRouter.get('/:username', checkAuth, (req,res) => {
  var username = req.params.username;

  User.find({ username }, (err, res_find) => {
    if(err) throw err;
    var user_id = res_find[0]._id;

    Cart.find({ user: user_id }, (errCart,res_findCart) => {
      if (errCart) {
        res.json({ "resultType": "failure", "resultMessage": errCart });
      } else if (res_findCart.length === 0) {
        res.json({ "resultType": "success", "resultMessage": "No existing cart was found.", "cart": JSON.stringify([]) });
      } else {
        res.json({ "resultType": "success", "resultMessage": "An existing cart was found.", "cart": JSON.stringify(res_findCart[res_findCart.length-1].cart) });
      }
    })
  })
})

cartRouter.post('/:username', checkAuth, (req,res) => {
  var username = req.params.username;
  var cart = req.body.cart;

  User.find({ username }, (err, res_find) => {
    if(err) {
      res.json({ "resultType": "failure", "resultMessage": err.message });
    } else {
      if (res_find.length !== 0) {
        var user_id = res_find[0]._id;
        var options = { upsert: true, new: true, setDefaultsOnInsert: true };

        Cart.findOneAndUpdate({ user: user_id }, { cart }, options, (err_cartFind, res_cartFind) => {
          if (err_cartFind) {
            res.json({ "resultType": "failure", "resultMessage": err_cartFind.message });
          } else {
            res.json({ "resultType": "success", "resultMessage": "Cart successfully updated." });
          }
        })
      } else {
        res.json({ "resultType": "failure", "resultMessage": "User wasn't found." });
      }
    }
  })
})

module.exports = cartRouter;
