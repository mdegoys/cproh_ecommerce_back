var express = require('express');
var mongoose = require('mongoose');
var cors = require('cors');
var userRouter = require('./userRouter');
var cartRouter = require('./cartRouter');
var app = express();

mongoose.connect('mongodb://localhost/cproh_ecommerce', {useNewUrlParser: true});

var products = [
  { "name" : "Bougie Frida Kahlo", "image": "https://images.asos-media.com/products/frida-kahlo-x-flamingo-candles-bougie-cactus-et-frangipanier-dans-une-boite-en-metal/12204475-1-multi?$n_320w$&wid=317&fit=constrain", "description": "Frida Kahlo x Flamingo Candles - Bougie cactus et frangipanier dans une boîte en métal", "price": 13.99 },
  { "name" : "Panier à linge sale", "image": "https://images.asos-media.com/products/moxon-i-like-it-dirty-panier-a-linge-sale/11827073-1-multi?$n_320w$&wid=317&fit=constrain", "description": "MOXON - I like it dirty - Panier à linge sale", "price": 24.99 },
  { "name" : "Tasse", "image": "https://images.asos-media.com/products/typo-x-the-simpsons-duff-beer-tasse/11870739-1-multi?$n_320w$&wid=317&fit=constrain","description": "Typo x The Simpsons - duff beer - Tasse", "price": 9.99 },
  { "name" : "Gourde à motif", "image": "https://images.asos-media.com/products/fizz-hydrate-gourde-a-motif-poisson-globe-et-inscription/11916423-1-multi?$n_320w$&wid=317&fit=constrain", "description": "Fizz - Hydrate - Gourde à motif poisson-globe et inscription", "price": 7.49 },
  { "name" : "Petit pot", "image": "https://images.asos-media.com/products/sass-belle-petit-pot-motif-paresseux/11245957-1-multi?$n_320w$&wid=317&fit=constrain", "Sass & Belle - Petit pot motif paresseux": "description du produit", "price": 15.99 },
]

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get('/products', (req, res) => {
  res.send(products);
})

app.use('/users', userRouter);
app.use('/carts', cartRouter);

app.listen(1234, () => {
  console.log('coucou les copains');
});
