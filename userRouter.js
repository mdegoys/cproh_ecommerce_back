var express = require('express');
var jwt = require('jsonwebtoken');
var userRouter = express.Router();
var User = require('./userModel.js');

userRouter.post('/login', (req,res) => {
  var username = req.body.username;
  var password = req.body.password;

  User.find({ username, password }, (err,find_res) => {
    if (err) {
      res.json({ "resultType": "failure", "resultMessage": err });
    } else if (find_res.length === 0) {
      res.json({ "resultType": "failure", "resultMessage": "Wrong credentials." });
    } else {
      var token = jwt.sign({ username }, 'shhhhh');
      res.json({ "resultType": "success", "resultMessage": "You are successfully logged in.", "token": token });
    }
  })
})

userRouter.post('/register', (req,res) => {
  var newUser = new User(req.body)

  newUser.save((err, user) => {
    if (err) {
      res.json({ "resultType": "failure", "resultMessage": err });
    } else {
      res.json({ "resultType": "success", "resultMessage": "Account successfully created." });
    }
  })
})



module.exports = userRouter;
